package wit.org.mytweet.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

import wit.org.mytweet.R;
import wit.org.mytweet.android.helpers.ContactHelper;
import wit.org.mytweet.app.MyTweetApp;
import wit.org.mytweet.models.Tweet;
import wit.org.mytweet.models.TweetList;

import static wit.org.mytweet.android.helpers.IntentHelper.navigateUp;
import static wit.org.mytweet.android.helpers.IntentHelper.sendEmail;


public class TweetFragment extends Fragment implements TextWatcher, OnClickListener
{
    public static   final String  EXTRA_TWEET_ID = "mytweet.TWEET_ID";

    private static  final int     REQUEST_CONTACT = 1;

    private TextView numberCounterText;
    private EditText tweetText;
    private ImageButton tweetButton;
    private Button contactButton;
    private Button emailButton;

    private Tweet tweet;
    private TweetList tweetList;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        UUID tweId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);

        MyTweetApp app = (MyTweetApp) getActivity().getApplication();
        tweetList = app.tweetList;
        tweet = tweetList.getTweet(tweId);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        updateControls(tweet);

        return v;
    }

    private void addListeners(View v)
    {
        numberCounterText = (TextView) v.findViewById(R.id.numberCounter);
        tweetText  = (EditText)   v.findViewById(R.id.tweetText);
        contactButton = (Button)  v.findViewById(R.id.contactButton);
        emailButton = (Button)  v.findViewById(R.id.emailButton);
        tweetButton = (ImageButton)  v.findViewById(R.id.tweetButton);


        tweetButton.setOnClickListener(this);
        emailButton.setOnClickListener(this);
        contactButton.setOnClickListener(this);
        tweetText.addTextChangedListener(this);

    }

    public void updateControls(Tweet tweet)
    {
        numberCounterText.setText(String.valueOf(tweet.numberCounter));
        tweetText.setText(tweet.tweetText);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home: navigateUp(getActivity());
                return true;
            default:                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        tweetList.saveTweets();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        else
        if (requestCode == REQUEST_CONTACT)
        {
            String name = ContactHelper.getContact(getActivity(), data);
            tweet.contact = name;
            contactButton.setText(name);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        numberCounterText.setText(String.valueOf(tweet.numberCounter - s.length()));
    }

    @Override
    public void afterTextChanged(Editable c)
    {
        tweet.setTweetText(c.toString());
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tweetButton:
                Toast toast = Toast.makeText(getActivity(), "Tweet Sent", Toast.LENGTH_SHORT);
                toast.show();
                break;

            case R.id.contactButton:
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                if (tweet.contact != null)
                {
                    contactButton.setText(tweet.contact);
                }
                break;

            case R.id.emailButton:
                sendEmail(getActivity(), tweet.contact, getString(R.string.subject), tweet.getTweet());
                break;

        }
    }

}