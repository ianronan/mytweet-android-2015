package wit.org.mytweet.android.helpers;

import android.util.Log;

/**
 * Created by ianie on 27/10/2015.
 */
public class LogHelpers
{
    public static void info(Object parent, String message)
    {
        Log.i(parent.getClass().getSimpleName(), message);
    }
}
