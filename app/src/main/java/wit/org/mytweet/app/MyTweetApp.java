package wit.org.mytweet.app;
import static wit.org.mytweet.android.helpers.LogHelpers.info;

import wit.org.mytweet.models.TweetList;
import wit.org.mytweet.models.TweetListSerializer;

import android.app.Application;

public class MyTweetApp extends Application
{
    private static final String FILENAME = "tweetList.json";

    public TweetList tweetList;

    @Override
    public void onCreate()
    {
        super.onCreate();
        TweetListSerializer serializer = new TweetListSerializer(this, FILENAME);
        tweetList = new TweetList(serializer);

        info(this, "MyTweet app launched");
    }
}